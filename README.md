# Installation
## Conda environment
```bash
$ conda env create -f Projet_4A.yml
$ conda activate Projet_4A
```
## Configure dependencies
<ol>
<li>
Go to the following path

```
ANACONDA_BASE_DIR/lib/pythonX.X/site-packages
```
</li>
<li>
Create a .pth file in which you specify the paths to the local dependencies
</li>
<li>
Restart the anaconda environment
</li>
</ol>

# Use case
## Launching the sevice
You only need to open and run the interface notebook cells which starts the server. The link at the end redirects you to the application.

# License
[LGPL3](https://choosealicense.com/licenses/lgpl-3.0/)
