json_data = [
    {
        id: 0,
        start: 209.99,
        end: 239.5,
        color: '#68C1F04D'

    }, {
        id: 1,
        start: 240.07,
        end: 260.01,
        color: '#68C1F04D'

    }, {
        id: 2,
        start: 261.18,
        end: 279.09,
        color: '#68C1F04D'

    }, {
        id: 3,
        start: 279.91,
        end: 280.8,
        color: '#C7090E4D'

    }, {
        id: 4,
        start: 280.8,
        end: 285.18,
        color: '#C7090E4D'

    }, {
        id: 5,
        start: 280.8,
        end: 285.18,
        color: '#2124A94D'

    }, {
        id: 6,
        start: 285.18,
        end: 288.74,
        color: '#C7090E4D'

    }, {
        id: 7,
        start: 288.74,
        end: 294.1,
        color: '#C7090E4D'

    }, {
        id: 8,
        start: 288.74,
        end: 294.1,
        color: '#2124A94D'

    }, {
        id: 9,
        start: 294.1,
        end: 294.63,
        color: '#C7090E4D'

    },
]

wavFile = 'assets/Antonio_Vivaldi_les_quatre_saisons.wav';
container = '#waveform_container';
colorWave = '#cccecf';
colorProgress = '#2791e3';
let wavesurfer;

function create_waveform(wavFile, data, container, colorWave='#cccecf', colorProgress='#2791e3') {
    wavesurfer = WaveSurfer.create({
        container: container,
        waveColor: colorWave,
        progressColor: colorProgress,
        plugins: [
            WaveSurfer.regions.create({
                regions: data
            })

        ]
    });

    wavesurfer.load(wavFile);

    var slider = document.querySelector('#slider');

    slider.oninput = function () {
        var zoomLevel = Number(slider.value);
        wavesurfer.zoom(zoomLevel);
    }

    const bouton = document.getElementById('play_wav');
    bouton.addEventListener('click', (event) => {
        wavesurfer.playPause();
    });
}

create_waveform(wavFile, json_data, container);