#!/bin/bash

# Initializing script functions
DOWNLOAD_TARGET_DIR=$HOME
CONDA_ENV_NAME="DHL_LIUM"
PROJECT_DIR=$PWD
CONDA_URL=
OS_ARCH=
FILE=

Help()
{
   # Display Help
   echo "This script is used to setup the conda environment needed for the project. All the dependencies used will be downloaded and installed automatically."
   echo
   echo "Syntax: ./install.sh [-h] [-m [<download_path>]] [-e [<env_name>]] [-p [<env_name>]] [-d [<path>]]"
   echo "options:"
   echo "-h     Display this Help."
   echo "-m     Install Miniconda only. The optional argument download_path is used if you want to specify where to download the temporary Miniconda shell script."
   echo "-e     Create a new conda environment and install needed packages without PyTorch. The default name for the new environment is DHL_LIUM, specify env_name to personnalize the name of the environment."
   echo "-p     Install PyTorch only. If env_name is not specified, the used environment is the default DHL_LIUM."
   echo "-d     Get a copy of the project directory. Specify a path in where the directory will be put, otherwise the current working directory will be used."
   echo
   echo "Note: To run the complete setup, please don't specify any of the above options"
}

Miniconda()
{
    # Detect the user's OS and its Architecture to choose the appropriate Miniconda installer
    OS_ARCH=$(uname -m)

    if [[ "$OSTYPE" == "linux-gnu"* ]]
    then
        CONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-$OS_ARCH.sh"
        FILE="Miniconda_installer.sh"
    elif [[ "$OSTYPE" == "darwin"* ]]
    then
        CONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-MacOSX-$OS_ARCH.sh"
        FILE="Miniconda_installer.sh"
    elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]
    then
        CONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Windows-$OS_ARCH.exe"
        FILE="Miniconda_installer.exe"
    else
        echo "ERROR: Unsupported operating system. Aborting execution ..."
        exit 1
    fi

    # Check if miniconda already installed, if not install it
    if [ -x "$(command -v c conda)" ]
    then
        echo "Miniconda already installed"
    else
        if [ -f "$DOWNLOAD_TARGET_DIR/$FILE" ]
        then
            echo "INFO: $FILE already downloaded."
        else
            echo " ================== Downloding Miniconda ================== "
            curl -XGET $CONDA_URL -o "$DOWNLOAD_TARGET_DIR/$FILE"
            if [ $? -ne 0 ]
            then
                rm "$DOWNLOAD_TARGET_DIR/$FILE"
            fi
        fi
        echo " ================== Installing Miniconda ================== "
        if [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]
        then
            $DOWNLOAD_TARGET_DIR/$FILE
        else
            bash "$DOWNLOAD_TARGET_DIR/$FILE" -b -p $HOME/miniconda3/
        fi
        if [ $? -ne 0 ]
        then
            echo "ERROR: Installation failed."
            exit 1
        else
            echo "SUCCESS: Installation finished."
            if [[ "$OSTYPE" == "linux-gnu"* ]]
            then
                source "$HOME/.bashrc"
            elif [[ "$OSTYPE" == "darwin"* ]]
            then
                source "$HOME/.zshrc"
            else
                echo "ERROR: Unsupported operating system. Aborting execution ..."
                exit 1
            fi
            rm "$DOWNLOAD_TARGET_DIR/$FILE"
        fi
    fi
}

Create_Environment() {
    # Check if an environment with the provided name is already existing
    # If not, create new conda env and install needed packages without PyTorch
    if conda env list | grep -q "$CONDA_ENV_NAME"
    then
        echo "WARNING: $CONDA_ENV_NAME evironment already exists"
        CHOICE="N"
        read -p "Proceed with the existing environment? (Y/[N]) " CHOICE
        if [ "$CHOICE" = "Y" ]
        then
            echo "Proceeding with Environment $CONDA_ENV_NAME"
        else
            read -p "Enter a new Environment name: " CONDA_ENV_NAME
        fi
    else
        echo " ============== Creating Conda Environment =============== "
        yes | conda create -n $CONDA_ENV_NAME python=3.9
        echo "SUCCESS: Environment '$CONDA_ENV_NAME' has been created"
    fi

    echo " =============== Installing Conda Packages =============== "
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge jupyterlab
    yes | conda install -n $CONDA_ENV_NAME -c anaconda django
    yes | conda install -n $CONDA_ENV_NAME -c anaconda lxml
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge matplotlib
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge h5py
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge pandas
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge pysoundfile
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge tqdm
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge pyyaml
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge scikit-learn
    yes | conda install -n $CONDA_ENV_NAME tensorboard
    yes | conda install -n $CONDA_ENV_NAME tabulate
    yes | conda install -n $CONDA_ENV_NAME filelock
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge gdown
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge sqlalchemy
    yes | conda install -n $CONDA_ENV_NAME -c conda-forge librosa
    yes | conda update --all
    yes | conda install -c anaconda conda-build
    PyTorch

    git clone https://gitlab.com/Rd-Massou/dhl_dependencies.git $HOME/DHL_dependencies

    echo " ============= Finalizing Conda Dependencies ============= "
    conda develop -n $CONDA_ENV_NAME $HOME/DHL_dependencies
    echo "SUCCESS: Environment $CONDA_ENV_NAME is ready"
}

PyTorch() {
    # Check if pytorch is already installed
    if conda list pytorch -n $CONDA_ENV_NAME | grep -q "pytorch"
    then
        echo "PyTorch already installed"
    else
        # Install PyTorch depending on user's Processing Kit choice
        CHOICE="CPU"
        read -p "Choose processing kit: ([CPU]/GPU) " CHOICE
        if [ "$CHOICE" = "CPU" ]
        then
            echo " ================= Installing CPU PyTorch ================ "
            if [[ "$OSTYPE" == "linux-gnu"* ]]
            then
                yes | conda install pytorch torchvision torchaudio cpuonly -n $CONDA_ENV_NAME -c pytorch
            elif [[ "$OSTYPE" == "darwin"* ]]
            then
                yes | conda install pytorch torchvision torchaudio -c pytorch -n $CONDA_ENV_NAME
            elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]
            then
                yes | conda install pytorch torchvision torchaudio cpuonly -c pytorch -n $CONDA_ENV_NAME
            else
                echo "ERROR: Unsupported operating system. Aborting execution ..."
                exit 1
            fi
            echo "SUCCESS: CPU PyTorch finished installing"
        elif [ "$CHOICE" = "GPU" ]
        then
            if [[ "$OSTYPE" == "linux-gnu"* ]]
            then
                yes | conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch -n $CONDA_ENV_NAME
            elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]
            then
                yes | conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch -n $CONDA_ENV_NAME
            else
                echo "ERROR: Unsupported operating system. Aborting execution ..."
                exit 1
            fi
            echo "SUCCESS: CUDA PyTorch finished installing"
        else
            CHOICE="Y"
            read -p "ERROR: Choice not supported, try again? ([Y]/N) " CHOICE
            if [ "$CHOICE" = "Y" ]
            then
                PyTorch
            else
                echo "INFO: Aborting PyTorch Installation"
                exit 1
            fi
            
        fi
    fi
}

Get_Project_Dir() {
    # Get a copy of the project directory
    echo " ============== Cloning Project Repository =============== "
    REPO_URL="https://gitlab.com/ia_ensim/deep-human-learning.git"
    git clone $REPO_URL $PROJECT_DIR/Deep_Human_Learning
}

Wrap_Up() {
    echo " ==================== Setup Finished ===================== "
    CHOICE="Y"
    read -p "Do you want to launch the application ? ([Y]/N) " APP_LAUNCH

    if [ "$APP_LAUNCH" = "Y" ]
    then
        echo " ================= Launching Application ================= "
        srun -N 1 -n 1 --mem 10G --time=0-12:00 jlaunch jupyter lab
    fi
}

# Handeling options
while getopts hmepd option
do
    case $option in
        d) # Get a copy of the project's directory
            if [ ! -z "$2" ]
            then
                PROJECT_DIR=$2
            fi
            Get_Project_Dir
            exit 0;;
        h) # display Help
            Help
            exit 0;;
        m) # Install Miniconda
            if [ ! -z "$2" ]
            then
                DOWNLOAD_TARGET_DIR=$2
            fi
            Miniconda
            exit 0;;
        e) # Create conda env and install needed packages without PyTorch
            if [ ! -z "$2" ]
            then
                CONDA_ENV_NAME=$2
            fi
            Create_Environment
            exit 0;;
        p) # Install Install PyTorch only
            if [ ! -z "$2" ]
            then
                CONDA_ENV_NAME=$2
            fi
            PyTorch
            exit 0;;
        \?) # Invalid option
            echo "Error: Invalid option"
            echo "To get help, try ./install.sh -h"
            exit 1;;
    esac
done

# CONDA
Miniconda

# Create the new environment
Create_Environment

# Get the project directory to the current path
Get_Project_Dir

# Wrap up the whole setup
Wrap_Up