"""App URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
# -*- coding: utf-8 -*-
from django.conf.urls import include#, url
from django.urls import path
#from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static

#Django n'accede pas au MEDIA_ROOT par default car c'est dangereux dans un environment de production
#Mais en developpement on utilise la derniere ligne pour que Django utilise MEDIA_URL

urlpatterns = [
    #    path('admin/', admin.site.urls),
    path('', include('Wav_Service.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
