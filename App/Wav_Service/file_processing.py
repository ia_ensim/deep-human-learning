import random
import wave
import contextlib
import pandas as pd
import sidiar
import torch

MODEL_PATH = 'Wav_Service/seqtoseq_tcn_best.pt'
def wav_predict(wavs):
    if torch.cuda.is_available():
        device = 'cuda'
    else:
        device = 'cpu'
    checkpoint = torch.load(MODEL_PATH, map_location=device)
    model_opts = checkpoint["model_archi"]
    model = sidiar.nn_models.sequence_models.SeqToSeq(model_opts)
    th_in, th_out = 0.5, 0.5
    wavs = '/info/etu/slbm/e2105046/deep-human-learning/App/Wav_Service/static/Wav_Service/uploads/wav/' + wavs
    pred_diar = sidiar.sequence2sequence.seqtoseq_predict.infer(wavs, model=model, device=device)
    return pred_diar

def dummy_diarization(wav_path, n_speakers, n_segments=100):
    """
    dummy_diarization(n_speakers, wav_length, n_segments=100)
    :param wav_path: (str) Le chemin vers le fichier auquel on génère la diarization
    :param n_speakers: (int) Le nombre de speaker qui interviennent dans le wav
    :param n_segments: (int) Le nombre de segments à générer en sortie
    :return: gen_data: (pandas.DataFrame) l'ensemble de segments généré

    Cette fonction genère une DataFrame des segments fictifs qui reprèsent les différentes interventions de chaque speaker dans un audio. chaque segments est d'une durée variante entre 5 et 60 secondes.
    """
    wav_path = '/info/etu/slbm/e2105046/deep-human-learning/App/Wav_Service/static/Wav_Service/uploads/wav/' + wav_path
    with contextlib.closing(wave.open(wav_path,'r')) as wav_file:
        # Liste bidon des speakers qui interviennent dans le wav
        speakers = ["speaker_{}".format(i) for i in range(1, n_speakers + 1)]

        # Generation aleatoire de segments
        frames = wav_file.getnframes()
        rate = wav_file.getframerate()
        duration = frames / float(rate)
        seg_speaker = [speakers[int(len(speakers) * random.random())] for i in range(n_segments)]
        start = [duration * random.random() for i in range(n_segments)]
        gen_data = pd.DataFrame({'cluster': seg_speaker, 'start': start})
        gen_data['stop'] = gen_data['start'] + 5 + random.random() * 60
    return gen_data

def process_file(data):
    # On génère les couleurs des differents speakers
    data = data[['cluster', 'start', 'stop']]
    data['cluster'] = [" ".join(i.split('_')).title() for i in data['cluster']]
    speakers = list(set(data['cluster']))
    colors = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)]) + '4D' for i in speakers]
    colors_dict = dict(zip(speakers, colors))

    # On restructure les données du DataFrame pour qu'elle soit exploitable par l'interface
    data["color"] = data["cluster"]
    data['start'] = [round(value, 2) for value in data['start']]
    data['stop'] = [round(value, 2) for value in data['stop']]
    data.replace({'color': colors_dict}, inplace=True)
    data['id'] = list(range(1, data.shape[0] + 1))
    data.columns = ['speaker', 'start', 'end', 'color', 'id']
    return data, colors_dict
