from django.apps import AppConfig


class WavServiceConfig(AppConfig):
    name = 'Wav_Service'
