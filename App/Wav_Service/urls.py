# -*- coding: utf-8 -*-
from django.conf.urls import include#, url
from django.urls import path
from . import views

#POur que les views soient accessibles, il faut spécifier leurs urls respectifs

urlpatterns = [
    path('app/', views.index, name='app_index'),
    path('', views.listing, name='listing')
]