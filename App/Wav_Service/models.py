# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
class MyModel(models.Model):
    # file will be uploaded to MEDIA_ROOT/wav
    docfile = models.FileField(upload_to='wav/')
#le filefield store le doc dans media/...