let wavFile = document.getElementById('wavFile').value;
let WaveContainer = '#waveform_container';
let colorWave = '#cccecf';
let colorProgress = '#2791e3';
let wavesurfer;

function loadSegment(componentId, region) {
    let container = document.createElement("div");
    container.setAttribute("class", "shadow-sm d-flex justify-content-between mb-1 p-1");

    let segmentNumber = document.createElement("div");
    segmentNumber.setAttribute("style", "color:white;");
    segmentNumber.innerText = "Segment " + region.id;
    container.appendChild(segmentNumber);

    let startTime = document.createElement("div");
    startTime.setAttribute("style", "color:white;");
    startTime.innerText = region.start;
    container.appendChild(startTime);

    let endTime = document.createElement("div");
    endTime.setAttribute("style", "color:white;");
    endTime.innerText = region.end;
    container.appendChild(endTime);

    let btnPlay = document.createElement("button");
    btnPlay.setAttribute("class", "btn-sm btn-primary");

    let faIcon = document.createElement("i");
    faIcon.setAttribute("class", "fas fa-play");
    faIcon.setAttribute("style", "color:white;");
    btnPlay.appendChild(faIcon);
    container.appendChild(btnPlay);

    btnPlay.addEventListener("click", function(){
        wavesurfer.play(region.start, region.end);
    });
    document.getElementById(componentId).appendChild(container);
}

function speakerEventListener(selectorId, containerId) {
    let dropDownMenu = document.getElementById(selectorId)
    dropDownMenu.addEventListener("change", () => {
        let container = document.getElementById(containerId);
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }

        wavesurfer.clearRegions();

        if(dropDownMenu.value === "") {
            let banner = document.createElement("div");
            banner.setAttribute("class", "alert alert-warning");
            banner.innerText = "No speaker selected! Please select a speaker to see his/her segments";
            container.appendChild(banner);
            for(let segment of jsonData) {
                wavesurfer.addRegion({
                    id : segment.id,
                    start: segment.start,
                    end: segment.end,
                    color: segment.color
                });
            }
        } else {
            for(let segment of jsonData) {
                if (dropDownMenu.value === segment.speaker) {
                    loadSegment( containerId, segment);
                    wavesurfer.addRegion({
                        id : segment.id,
                        start: segment.start,
                        end: segment.end,
                        color: segment.color
                    });
                }
            }
        }
    });
}

function createWaveform(wavFile, data, container, colorWave='#cccecf', colorProgress='#2791e3') {
    wavesurfer = WaveSurfer.create({
        container: container,
        waveColor: colorWave,
        progressColor: colorProgress,
        plugins: [
            WaveSurfer.regions.create({
                regions: data
            })

        ]
    });
    console.log(wavFile);

    wavesurfer.load(wavFile);

    wavesurfer.on('region-update-end', function(region, event){  
        console.log("entered method: region-update-end");
    
        if(!region.hasDeleteButton) {
            var regionEl = region.element;
        
            var deleteButton = regionEl.appendChild(document.createElement('deleteButton'));
            deleteButton.className = 'fa fa-trash';
        
            deleteButton.addEventListener('click', function(e) {
                region.remove();
            });
            
            deleteButton.title = "Delete region";
            
            var css = {
            display: 'block',
                float: 'right',
                padding: '3px',
                position: 'relative',
                zIndex: 10,
                cursor: 'pointer',
                cursor: 'hand',
                color: '#129fdd'
            };
            
            region.style(deleteButton, css);
            region.hasDeleteButton = true;    
        }
    });


    wavesurfer.on('ready', function () {
        // Enable creating regions by dragging
        wavesurfer.enableDragSelection({});
    });

    let slider = document.querySelector('#zoomRange');

    slider.oninput = function () {
        let zoomLevel = Number(slider.value);
        wavesurfer.zoom(zoomLevel);
    }

    const bouton = document.getElementById('play_wav');
    bouton.addEventListener('click', (event) => {
        wavesurfer.playPause();
    });
}

document.addEventListener("DOMContentLoaded", function() {
    createWaveform(wavFile, jsonData, WaveContainer);

    speakerEventListener('selectSpeaker1', 'segmentContainer1');
    speakerEventListener('selectSpeaker2', 'segmentContainer2');
});
