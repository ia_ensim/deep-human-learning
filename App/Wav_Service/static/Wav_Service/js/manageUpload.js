function clickEventListener(btnId, inputId, formId) {
    let button = document.getElementById(btnId);
    let fieldToCheck = document.getElementById(inputId);
    let form = document.getElementById(formId);

    button.addEventListener('click', (event) => {
        if (fieldToCheck.value === "") {
            let className = form.getAttribute("class");
            className += " was-validated";
            form.setAttribute("class", className);
            fieldToCheck.setAttribute("required", true);
            event.preventDefault();
        }
    })
}

document.addEventListener("DOMContentLoaded", function() {
    clickEventListener("validate_choice", "file_choice", "choiceForm");
    clickEventListener("upload_file", "uploaded_file", "uploadForm");
});