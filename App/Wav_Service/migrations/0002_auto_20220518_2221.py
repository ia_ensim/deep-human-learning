# Generated by Django 2.2.5 on 2022-05-18 20:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Wav_Service', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mymodel',
            old_name='upload',
            new_name='docfile',
        ),
    ]
