from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
import json
from os import listdir

from django.utils.text import get_valid_filename

from .file_processing import *

# Create your views here.

#les views permettent le contact avec le front
def listing(request):
    # Handle file upload
    if "validate_choice" in request.POST:
        wav_choice = request.POST["chosen_wav_file"]
        if wav_choice != "":
            return redirect('app/?file=' + wav_choice)
    if "upload_file" in request.POST:
        uploaded_file = request.FILES["uploaded_file"]
        if uploaded_file:
            fs = FileSystemStorage()
            path = "/info/etu/slbm/e2105046/deep-human-learning/App/Wav_Service/static/Wav_Service/uploads/wav/" + get_valid_filename(uploaded_file.name)
            fs.save(path, uploaded_file)
            return redirect('app/?file=' + get_valid_filename(uploaded_file.name))

    # Load documents for the list page
    wav_folder = "/info/etu/slbm/e2105046/deep-human-learning/App/Wav_Service/static/Wav_Service/uploads/wav"
    files_list = [f for f in listdir(wav_folder)]

    #retourner l'upload
    return render(
        request,
        'WavService/listing.html',
        {'files': files_list}
    )


def index(request):
    process_wav_file = request.GET.get('file')
    trs = wav_predict(process_wav_file)
    data, colors_dict = process_file(trs)
    context = {
        'json_data': json.dumps(data.to_dict(orient='records')),
        'speakers': colors_dict,
        'wav_file': process_wav_file
    }
    return render(request, "WavService/index.html", context)
