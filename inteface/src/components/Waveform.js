import React, { Component } from "react";
import MultiCanvas from "wavesurfer.js";

class Waveform extends Component {
  state = {
    playing: false
  };

  componentDidMount() {
    const track = document.querySelector("#track");

    canvasContainer = document.createElement("div");
    this.waveform = MultiCanvas(canvasContainer, null);

    render() {
      const url = require("../wav/20120130.1327.LCP_LCPInfo13h30.wav");

      return (
        <div id="canvas-container">
          <button>
            {!this.state.playing ? "Play" : "Pause"}
          </button>
          <div id="waveform" />
          <audio id="track" src={url} />
          <div>0.52</div>
        </div>
      );
    }
  }
}

export default Waveform;